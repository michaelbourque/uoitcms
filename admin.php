<?php

define('INCLUDE_CHECK',true);

require 'connect.php';
require 'functions.php';
// These two files can be included only if INCLUDE_CHECK is defined


session_name('ws_session');
// Starting the session

session_set_cookie_params(2*7*24*60*60);
// Making the cookie live for 2 weeks

session_start();

if($_SESSION['isAdmin'] != 1) 
{
	header("Location: index.php");
}

if($_SESSION['id'] && !isset($_COOKIE['wsRemember']) && !$_SESSION['rememberMe'])
{
	// If the user is logged in but does not have the 'Remember Me' selected, destory the session

	$_SESSION = array();
	session_destroy();
	
	// Destroy the session
}


if(isset($_GET['logoff']))
{
	$_SESSION = array();
	session_destroy();
	
	header("Location: index.php");
	exit;
}

if(isset($_GET['removepost']))
{
	mysql_query("DELETE FROM blogs WHERE id ='$_GET[removepost]'");
	header("Location: admin.php");
	
}

else if($_POST['submit']=='Delete') {

	mysql_query("DELETE FROM comments WHERE id ='$_POST[commentid]'");
	
}
else if($_POST['submit']=='Approve') {

	mysql_query("UPDATE comments SET approved='1' WHERE id ='$_POST[commentid]'");
	
}
$script = '';

$myQuery="SELECT * FROM blogs";
$allposts=mysql_query($myQuery, $con);
$postcount=mysql_num_rows($allposts);

$myQuery="SELECT * FROM blogs ORDER BY id LIMIT 1";
$firstpost=mysql_query($myQuery, $con);
				
while($row = mysql_fetch_array($firstpost))

$blogID=$row['id'];

if (! empty($_GET['postid'])) 
{
	$blogID=$_GET['postid'];
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Web&Script Programming Blog</title>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
<script src="actions.js" type="text/javascript"></script>
<link href="css/styles.css" rel="stylesheet" type="text/css" media="screen" />
<link href="css/panel.css" rel="stylesheet" type="text/css" media="screen" />
<link href="login_panel/css/slide.css" rel="stylesheet" type="text/css"  />
<?php echo $script; ?>
</head>

<body>




<!-- Panel -->
<div id="toppanel">
	<div id="panel">
		<div class="content clearfix">
			<div class="left">
				<h1>Web&Script Blogging - Content Managment Portal</h1>
				<h2>Assignment #2</h2>		
				<p class="grey">Michael Bourque - 100258740</p>
				<h2>This system employs:</h2>
				<p class="grey">HTML, HTML5, CSS, CSS3, JavaScript, PHP, JQuery, AJAX, and more.</p>
			</div>
            
            
            <?php
			
			if(!$_SESSION['id']):
			
			?>
            
			<div class="left">
			</div>
			<div class="left right">			
			</div>
            
            <?php
			
			else:
			
			?>
            
            <div class="left">
            
            <h1>Administrators panel</h1>
            
            <p>Your are logged in as an authorized member of this blog.</p>
            <a href="?logoff">Log off</a>
            
            </div>
            
            <div class="left right">
            </div>
            
            <?php
			endif;
			?>
		</div>
	</div> <!-- /login -->	

    <!-- The tab on top -->	
	<div class="tab">
		<ul class="login">
	    	<li class="left">&nbsp;</li>
	        <li>Hello <?php echo $_SESSION['username'] ? $_SESSION['username'] : 'Guest';?>!</li>
			<li class="sep">|</li>
			<li id="toggle">
				<a id="open" class="open" href="#"><?php echo $_SESSION['id']?'Open Panel':'Log In | Register';?></a>
				<a id="close" style="display: none;" class="close" href="#">Close Panel</a>			
			</li>
	    	<li class="right">&nbsp;</li>
		</ul> 
	</div> <!-- / top -->
	
</div> <!--panel -->




	<div id="container">
    	<div id="title"><h1>Web & Script Programming Blog - Content Management Portal</h1></div>
        <div id="subtitle"><h2>Michael Bourque - 100258740 - November 8th-12th, 2013<h2></div>
        <div id="navigation">
                 <a class="hmenu" href='admin.php?removepost=<? echo $blogID; ?>'>Delete</a>
                 <a class="hmenu" href='addpost.php'>Add Post</a>
                 <a class="hmenu" href='logs.php'>Logs</a>
                 <a class="hmenu" href="index.php">Home</a>
        </div>
        <div id="left">
        	<div id="blog">
            	<?php
					$myQuery="SELECT * FROM blogs WHERE id='" . $blogID ."'";
					$blog=mysql_Query($myQuery, $con);
				
					$myQuery="SELECT id FROM logs WHERE postid='$blogID'";
					$hitresult=mysql_Query($myQuery, $con);
					$hitcount=mysql_num_rows($hitresult);
				
            		while($row = mysql_fetch_array($blog))
					{
						echo '<div id="blog_title">' . $row['blog_title'] . '</div>';
                        echo '<div id="blog_author">by: ' . $row['author'] . '</div>';
						echo '<div id="hitcount">This post has been viewed ' . $hitcount . ' times.</div>';
						echo '<div id="blog_contents"><p>' . $row['blog_content'] . '</p></div>';
						$thisID=$row['id'];
					}
				?>
            </div>
            <div id="comments">   
            		<h3>Comments:</h3>
                	<?php
						$myQuery="SELECT * FROM comments WHERE blog_id='".$thisID."'";
						$commentsblog=mysql_Query($myQuery, $con);
				
            		while($row = mysql_fetch_array($commentsblog))
				    {               
                		echo '<div class="admincomment">';
						echo '<span class="commentauthor">'.$row['author'].'</span></br>';
						echo $row['content'];
	
						?>
        				<form action="" method="POST">
                            <input type="hidden" name="commentid" value="<?php echo $row['id'];?>"/>
       	            		<input type="submit" name="submit" value="Delete" class="admincommentbutton" />
						</form>
                        <?php
						if ($row['approved']<1){
						?>
                        <form id="approveform" action="" method="POST">
                            <input type="hidden" name="commentid" value="<?php echo $row['id'];?>"/>
       	            		<input type="submit" id="approve" name="submit" value="Approve" class="admincommentbutton" />
						</form>
                        <?php 
						}	
 						echo '</div>';		
					}
					?>
              
                    
            </div>
        </div>
        
        <div id="right">
        	<h2>Recent Blogs</h2>
       		<ul>
        	<?php
				$myQuery="SELECT id, blog_title FROM blogs ORDER BY id";
				$titleList=mysql_Query($myQuery, $con);
				
				while($row = mysql_fetch_array($titleList))
				{
					echo '<li><a class="bloglist" href="admin.php?postid='.$row['id'].'">'.$row['blog_title'].'</a></li>';
				}
			?>
            </ul>
        </div>
        <div id="shoutbox" class="shout-block">
        	<h2>Shout-outs:</h2>
            <br />
        	<?php
                $myQuery="SELECT * FROM shoutbox ORDER BY id DESC LIMIT 10";
				$shoutbox=mysql_Query($myQuery, $con);
				
				while($row = mysql_fetch_array($shoutbox))
				{ ?>
					<div class="shout-item">
						<div class="shout-post">
                			<span class="shoutauthor"><?php echo $row['username']; ?></span><span class="said"> | </span><span class="shoutout">  <?php echo nl2br($row['shout']);?></span>
                   		</div>
					</div>					
				<?php
				}
        	?>
        </div>

        <br class="clear" />
	</div>
</body>
</html>