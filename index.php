<?php

require 'connect.php';
require 'functions.php';

session_name('ws_session');
// Starting the session

session_set_cookie_params(2*7*24*60*60);
// Making the cookie live for 2 weeks

session_start();
// Start the session

if($_SESSION['id'] && !isset($_COOKIE['wsRemember']) && !$_SESSION['rememberMe'])
{
	// If the user is logged in but does not have the 'Remember Me' selected, destory the session

	$_SESSION = array();
	session_destroy();
	
	// Destroy the session
}


if(isset($_GET['logoff']))
{
	$_SESSION = array();
	session_destroy();
	
	header("Location: index.php");
	exit;
}



if($_POST['submit']=='Login')
{
	// Checking whether the Login form has been submitted
	
	$err = array();
	// Will hold our errors
	
	
	if(!$_POST['username'] || !$_POST['password'])
		$err[] = 'All the fields must be filled in!';
	
	if(!count($err))
	{
		$_POST['username'] = mysql_real_escape_string($_POST['username']);
		$_POST['password'] = mysql_real_escape_string($_POST['password']);
		$_POST['rememberMe'] = (int)$_POST['rememberMe'];
		
		// Escaping all input data

		$row = mysql_fetch_assoc(mysql_query("SELECT id,username,admin FROM users WHERE username='{$_POST['username']}' AND password='".md5($_POST['password'])."'"));

		if($row['username'])
		{
			// If everything is OK login
			
			$_SESSION['username']=$row['username'];
			$_SESSION['id'] = $row['id'];
			$_SESSION['rememberMe'] = $_POST['rememberMe'];
			
			// Store some data in the session
			
			setcookie('wsRemember',$_POST['rememberMe']);
			
			// If user is an admin, set the admin flag
			$_SESSION['isAdmin'] = $row['admin'];
		}
		else $err[]='Wrong username and/or password!';
	}
	
	if($err)
	$_SESSION['msg']['login-err'] = implode('<br />',$err);
	// Save the error messages in the session

	header("Location: index.php");
	exit;
}
else if($_POST['submit']=='Register')
{
	// If the Register form has been submitted
	
	$err = array();
	
	if(strlen($_POST['username'])<4 || strlen($_POST['username'])>32)
	{
		$err[]='Your username must be between 3 and 32 characters!';
	}
	
	if(preg_match('/[^a-z0-9\-\_\.]+/i',$_POST['username']))
	{
		$err[]='Your username contains invalid characters!';
	}
	
	if(!checkEmail($_POST['email']))
	{
		$err[]='Your email is not valid!';
	}
	
	if(!count($err))
	{
		// If there are no errors
		;
		
		$pass = substr(md5($_SERVER['REMOTE_ADDR'].microtime().rand(1,100000)),0,6);
		// Generate a random password
		
		$_POST['email'] = mysql_real_escape_string($_POST['email']);
		$_POST['username'] = mysql_real_escape_string($_POST['username']);
		// Escape the input data
		
		mysql_query("	INSERT INTO users(username,password,email)
						VALUES(
						
							'".$_POST['username']."',
							'".md5($pass)."',
							'".$_POST['email']."'						
						)");
		
		//if(mysql_affected_rows($con)==1)
		if (1)
		{
			send_mail(	'mbourque@webandscript.com',
						$_POST['email'],
						'Web & Script Blog Registration - Your New Password',
						'Your password is: '.$pass);

			$_SESSION['msg']['reg-success']='We sent you an email with your new password!';
		}
		else $err[]='This username is already taken!x'.$_POST['username'].''.$_POST['email'].''.$_POST['password'];
	}

	if(count($err))
	{
		$_SESSION['msg']['reg-err'] = implode('<br />',$err);
	}	
	
	header("Location: index.php");
	exit;
}
else if($_POST['submit']=='Shout') {
		if ($_SESSION['username']) {
			$author=$_SESSION['username'];
		} else {
			$author='Guest';
		}
		$shout=$_POST['shout'];
		$dbh = new PDO('mysql:dbname=ASS2;host=localhost', $dbuser, $dbpass);
        $myQuery = $dbh->prepare("INSERT INTO shoutbox (id,username,shout) VALUES ('',:authorLabel,:shoutLabel)");
		$myQuery->bindParam(':shoutLabel', $shout);
		$myQuery->bindParam(':authorLabel', $author);
		$myQuery->execute();
	
}


$script = '';

if($_SESSION['msg'])
{
	// The script below shows the sliding panel on page load
	
	$script = '
	<script type="text/javascript">
	
		$(function(){
		
			$("div#panel").show();
			$("#toggle a").toggle();
		});
	
	</script>';
	
}



//Set up pagination
$control=0;
$myQuery="SELECT * FROM blogs";
$allposts=mysql_query($myQuery, $con);
$postcount=mysql_num_rows($allposts);
$previousID=$nextID=null;
$postIDs=array();
$index=0;
while($row=mysql_fetch_array($allposts)) 
{
	$postIDs[$index]=$row['id'];
}

$myQuery="SELECT * FROM blogs ORDER BY id LIMIT 1";
$firstpost=mysql_query($myQuery, $con);
				
while($row = mysql_fetch_array($firstpost))

$blogID=$postIDs[$control];

if (! empty($_GET['postid'])) 
{
	$blogID=$_GET['postid'];
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Web&Script Programming Blog</title>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
<link href="css/styles.css" rel="stylesheet" type="text/css" media="screen" />
<link href="css/panel.css" rel="stylesheet" type="text/css" media="screen" />
<link href="login_panel/css/slide.css" rel="stylesheet" type="text/css"  media="screen" />
<?php echo $script; ?>
<script type="text/javascript" src="actions.js"></script>
</head>

<body>
<?php include('logit.php'); ?>



<!-- Panel -->
<div id="toppanel">
	<div id="panel">
		<div class="content clearfix">
			<div class="left">
				<h1>Web&Script Blogging</h1>
				<h2>Assignment #2</h2>		
				<p class="grey">Michael Bourque - 100258740</p>
				<h2>This system employs:</h2>
				<p class="grey">HTML, HTML5, CSS, CSS3, JavaScript, PHP, JQuery, AJAX, and more.</p>
			</div>
            
            
            <?php
			
			if(!$_SESSION['id']):
			
			?>
            
			<div class="left">
				<!-- Login Form -->
				<form class="clearfix" action="" method="post">
					<h1>Member Login</h1>
                    
                    <?php
						
						if($_SESSION['msg']['login-err'])
						{
							echo '<div class="err">'.$_SESSION['msg']['login-err'].'</div>';
							unset($_SESSION['msg']['login-err']);
						}
					?>
					
					<label class="grey" for="username">Username:</label>
					<input class="field" type="text" name="username" id="username" value="" size="23" />
					<label class="grey" for="password">Password:</label>
					<input class="field" type="password" name="password" id="password" size="23" />
	            	<label><input name="rememberMe" id="rememberMe" type="checkbox" checked="checked" value="1" /> &nbsp;Remember me</label>
        			<div class="clear"></div>
					<input type="submit" name="submit" value="Login" class="bt_login" />
				</form>
			</div>
			<div class="left right">			
				<!-- Register Form -->
				<form action="" method="post">
					<h1>Not a member yet? Sign Up!</h1>		
                    
                    <?php
						
						if($_SESSION['msg']['reg-err'])
						{
							echo '<div class="err">'.$_SESSION['msg']['reg-err'].'</div>';
							unset($_SESSION['msg']['reg-err']);
						}
						
						if($_SESSION['msg']['reg-success'])
						{
							echo '<div class="success">'.$_SESSION['msg']['reg-success'].'</div>';
							unset($_SESSION['msg']['reg-success']);
						}
					?>
                    		
					<label class="grey" for="username">Username:</label>
					<input class="field" type="text" name="username" id="username" value="" size="23" />
					<label class="grey" for="email">Email:</label>
					<input class="field" type="text" name="email" id="email" size="23" />
					<label>A password will be e-mailed to you.</label>
					<input type="submit" name="submit" value="Register" class="bt_register" />
				</form>
			</div>
            
            <?php
			
			else:
			
			?>
            
            <div class="left">
            
            <h1>Members panel</h1>
            
            <p>Your are logged in as an authorized member of this blog.</p>
            <a href="?logoff">Log off</a>
            
            </div>
            
            <div class="left right">
            </div>
            
            <?php
			endif;
			?>
		</div>
	</div> <!-- /login -->	

    <!-- The tab on top -->	
	<div class="tab">
		<ul class="login">
	    	<li class="left">&nbsp;</li>
	        <li>Hello <?php echo $_SESSION['username'] ? $_SESSION['username'] : 'Guest';?>!</li>
			<li class="sep">|</li>
			<li id="toggle">
				<a id="open" class="open" href="#"><?php echo $_SESSION['id']?'Open Panel':'Log In | Register';?></a>
				<a id="close" style="display: none;" class="close" href="#">Close Panel</a>			
			</li>
	    	<li class="right">&nbsp;</li>
		</ul> 
	</div> <!-- / top -->
	
</div> <!--panel -->




	<div id="container">
    	<div id="title"><h1>Web & Script Programming Blog</h1></div>
        <div id="subtitle"><h2>Michael Bourque - 100258740 - November 8th-12th, 2013<h2></div>
        <div id="navigation">
                 <?php
				 if ($_SESSION['isAdmin']==1) { ?>
                <a class="hmenu" href="admin.php">Admin</a>
                <?php } ?>
        </div>
        <div id="left">
        	<div id="blog">
            	<?php
					$myQuery="SELECT * FROM blogs WHERE id='" . $blogID ."'";
					$blog=mysql_Query($myQuery, $con);
				
            		while($row = mysql_fetch_array($blog))
					{
						echo '<div id="blog_title">' . $row['blog_title'] . '</div>';
                        echo '<div id="blog_author">by: ' . $row['author'] . '</div>';
						echo '<div id="blog_contents"><p>' . $row['blog_content'] . '</p></div>';
						$thisID=$row['id'];
					}
				?>
            </div>
<!-- Begin Comments -->
		<?php
        $myQuery="SELECT * FROM comments WHERE blog_id='".$thisID."' ORDER BY ID";
		$comment_query=mysql_Query($myQuery, $con);
		?>
		<div id="comments" class="comment-block">
		<h2>Comments</h2>
		<?php while($comment = mysql_fetch_array($comment_query)): ?>
        <?php if ($comment['approved']==1) { ?>
			<div class="comment-item">
				<div class="comment-post">
                	
					<span class="commentauthor"><?php echo $comment['author']; ?></span><span> said <?php echo nl2br($comment['content']);?></span>
                   
				</div>
			</div>
            <?php } ?>
		<?php endwhile?>
		</div>
		<br />
		<h2 class="commenth2">Submit new comment</h2>
		<!--comment form -->
		<form id="form" method="post">
			<!-- need to supply post id with hidden fild -->
			<input type="hidden" name="postid" value="<?php echo $thisID;?>">
            <input type="hidden" name="comauthor" value="<?php if($_SESSION['username']) { echo $_SESSION['username']; } else { echo 'Guest';}?>">
			<label>
			<textarea name="comment" id="comment" class="commentinput" placeholder="Type your comment here...." required></textarea>
			</label>
            <input type="submit" id="submit" value="Comment" class="commentbutton" />
		</form>
	

<!-- End Comments -->



        </div>
        
        <div id="right">
        	<h2>Recent Posts</h2>
       		<ul>
        	<?php
				$myQuery="SELECT id, blog_title FROM blogs ORDER BY id";
				$titleList=mysql_Query($myQuery, $con);
				
				while($row = mysql_fetch_array($titleList))
				{
					echo '<li><a class="bloglist" href="index.php?postid='.$row['id'].'">'.$row['blog_title'].'</a></li>';
				}
			?>
            </ul>
        </div>
        <div id="shoutbox" class="shout-block">
        	<h2>Shout-outs:</h2>
            <br />
        	<?php
                $myQuery="SELECT * FROM shoutbox ORDER BY id DESC LIMIT 10";
				$shoutbox=mysql_Query($myQuery, $con);
				
				while($row = mysql_fetch_array($shoutbox))
				{ ?>
					<div class="shout-item">
						<div class="shout-post">
                			<span class="shoutauthor"><?php echo $row['username']; ?></span><span class="said"> | </span><span class="shoutout">  <?php echo nl2br($row['shout']);?></span>
                   		</div>
					</div>					
				<?php
				}
        	?>
        </div>
            <div id="sendshout">
        		<h2>Send a Shout-out</h2>
                <br />
        		<form action="" id="shoutform" method="POST">
    				<input type="text" name="shout" value ="" class='shoutinput' />
                    <input type="hidden" name="shoutauthor" value="<?php if($_SESSION['username']) { echo $_SESSION['username']; } else { echo 'Guest';}?>">
		            <input type="submit" id="shoutsubmit" name="shoutsubmit" value="Shout" class="shoutbutton" />
				</form>
        	</div>

        <br class="clear" />
	</div>
</body>
</html>