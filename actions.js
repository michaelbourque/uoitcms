$(document).ready(function(){
	var form = $('#form');
	var submit = $('#submit');

	
	//On comment submit
	submit.click(function(e){		
		// prevent default action
		e.preventDefault();
		// send ajax request
		$.ajax({
			url: 'ajax_comment.php',
			type: 'POST',
			cache: false,
			data: form.serialize(), //form serialize data
			beforeSend: function(){
				// change submit button value text and disabled it
				submit.val('Submitting...').attr('disabled', 'disabled');
			},
			success: function(data){
				// Append with fadeIn see http://stackoverflow.com/a/978731
				var item = $(data).hide().fadeIn(800);
				$('.comment-block').append(item);
				
				// reset form and button
				form.trigger('reset');
				submit.val('Comment').removeAttr('disabled');
				
			},
			error: function(e){
				alert(e);
			}
		});
	});
	
	// The following actions pertain to the login panel
	
	// Expand Panel
	$("#open").click(function(){
		$("div#panel").slideDown("slow");
	
	});	
	
	// Collapse Panel
	$("#close").click(function(){
		$("div#panel").slideUp("slow");	
	});		
	
	// Switch buttons from "Log In | Register" to "Close Panel" on click
	$("#toggle a").click(function () {
		$("#toggle a").toggle();
	});	
	
	// These actions are for the admin area
	
	var approveform = $('#approveform');
	var approve = $('#approve');
	//On comment submit
	approve.click(function(e){		
		// prevent default action
		e.preventDefault();
		// send ajax request
		$.ajax({
			url: 'ajax_approve.php',
			type: 'POST',
			cache: false,
			data: approveform.serialize(), //form serialize data
			success: function(data){
				// Removed with fadeOut see http://stackoverflow.com/a/978731
				approve.fadeOut(800);
			},
			error: function(e){
				alert(e);
			}
		});
	});
	
	// This section makes the Shout box Ajaxy
	
	var shoutform = $('#shoutform');
	var shoutsubmit = $('#shoutsubmit');
	//On shout submit
	shoutsubmit.click(function(e){		
		// prevent default action
		e.preventDefault();
		// send ajax request
		$.ajax({
			url: 'ajax_shout.php',
			type: 'POST',
			cache: false,
			data: shoutform.serialize(), //form serialize data
			beforeSend: function(){
				// change submit button value text and disabled it
				shoutsubmit.val('Submitting...').attr('disabled', 'disabled');
			},
			success: function(data){
				// Append with fadeIn see http://stackoverflow.com/a/978731
				var item = $(data).hide().fadeIn(800);
				$('.shout-block').append(item);
				
				// reset form and button
				shoutform.trigger('reset');
				shoutsubmit.val('Shout').removeAttr('disabled');
				
			},
			error: function(e){
				alert(e);
			}
		});
	});

});