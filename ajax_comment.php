<?php
if (isset( $_SERVER['HTTP_X_REQUESTED_WITH'] )):

include('connect.php');
	
if (!empty($_POST['comauthor']) AND !empty($_POST['comment']) AND !empty($_POST['postid'])) {
	$comauthor = mysql_real_escape_string($_POST['comauthor']);
	$comment = $_POST['comment'];
	$postId = mysql_real_escape_string($_POST['postid']);			
}

	$dbh = new PDO('mysql:dbname=ASS2;host=localhost', $dbuser, $dbpass);
    $myQuery = $dbh->prepare("INSERT INTO comments (author,content,blog_id) VALUES (:authorLabel,:contentLabel,:postidLabel)");
	$myQuery->bindParam(':authorLabel', $comauthor);
	$myQuery->bindParam(':contentLabel', $comment);
	$myQuery->bindParam(':postidLabel',$postId);
	$myQuery->execute();
?>

<div class="comment-item">
	<div class="comment-post">
		<span class="commentauthor"><?php echo $comauthor ?></span><span> said <?php echo nl2br($comment);?></span><br /><br />
        <span><strong><small>Your comment will be visible to other visitors once it has been approved.</small></strong></span>
	</div>
</div>

<?php
endif
?>