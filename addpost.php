<?php

define('INCLUDE_CHECK',true);

require 'connect.php';
require 'functions.php';
// These two files can be included only if INCLUDE_CHECK is defined


session_name('ws_session');
// Starting the session

session_set_cookie_params(2*7*24*60*60);
// Making the cookie live for 2 weeks

session_start();

if($_SESSION['isAdmin'] != 1) 
{
	header("Location: index.php");
}

if($_SESSION['id'] && !isset($_COOKIE['wsRemember']) && !$_SESSION['rememberMe'])
{
	// If the user is logged in but does not have the 'Remember Me' selected, destory the session

	$_SESSION = array();
	session_destroy();
	
	// Destroy the session
}


if(isset($_GET['logoff']))
{
	$_SESSION = array();
	session_destroy();
	
	header("Location: index.php");
	exit;
}


else if($_POST['submit']=='Save') {
		$author=$_SESSION['username'];
		$blogpost=$_POST['blogcontents'];
		$blogtitle=$_POST['blogtitle'];
		$dbh = new PDO('mysql:dbname=ASS2;host=localhost', $dbuser, $dbpass);
        $myQuery = $dbh->prepare("INSERT INTO blogs (id,blog_title,blog_content, author) VALUES ('',:titleLabel,:contentLabel,:authorLabel)");
		$myQuery->bindParam(':titleLabel', $blogtitle);
		$myQuery->bindParam(':contentLabel', $blogpost);
		$myQuery->bindParam(':authorLabel', $author);
		$myQuery->execute();
		
		header("Location: admin.php");
	
}


$script = '';

if($_SESSION['msg'])
{
	// The script below shows the sliding panel on page load
	
	$script = '
	<script type="text/javascript">
	
		$(function(){
		
			$("div#panel").show();
			$("#toggle a").toggle();
		});
	
	</script>';
	
}




$myQuery="SELECT * FROM blogs";
$allposts=mysql_query($myQuery, $con);
$postcount=mysql_num_rows($allposts);

$myQuery="SELECT * FROM blogs ORDER BY id LIMIT 1";
$firstpost=mysql_query($myQuery, $con);
				
while($row = mysql_fetch_array($firstpost))

$blogID=$row['id'];

if (! empty($_GET['postid'])) 
{
	$blogID=$_GET['postid'];
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Web&Script Programming Blog - Add Post</title>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
<script src="login_panel/js/slide.js" type="text/javascript"></script>
<link href="css/styles.css" rel="stylesheet" type="text/css" />
<link href="css/demo.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="login_panel/css/slide.css" media="screen" />
<?php echo $script; ?>
</head>

<body>




<!-- Panel -->
<div id="toppanel">
	<div id="panel">
		<div class="content clearfix">
			<div class="left">
				<h1>Web&Script Blogging - Add Post</h1>
				<h2>Assignment #2</h2>		
				<p class="grey">Michael Bourque - 100258740</p>
				<h2>This system employs:</h2>
				<p class="grey">HTML, HTML5, CSS, CSS3, JavaScript, PHP, JQuery, AJAX, and more.</p>
			</div>
            
            
            <?php
			
			if(!$_SESSION['id']):
			
			?>
            
			<div class="left">
			</div>
			<div class="left right">			
			</div>
            
            <?php
			
			else:
			
			?>
            
            <div class="left">
            
            <h1>Members panel</h1>
            
            <p>Your are logged in as an authorized member of this blog.</p>
            <a href="">Log off</a>
            
            </div>
            
            <div class="left right">
            </div>
            
            <?php
			endif;
			?>
		</div>
	</div> <!-- /login -->	

    <!-- The tab on top -->	
	<div class="tab">
		<ul class="login">
	    	<li class="left">&nbsp;</li>
	        <li>Hello <?php echo $_SESSION['username'] ? $_SESSION['username'] : 'Guest';?>!</li>
			<li class="sep">|</li>
			<li id="toggle">
				<a id="open" class="open" href="#"><?php echo $_SESSION['id']?'Open Panel':'Log In | Register';?></a>
				<a id="close" style="display: none;" class="close" href="#">Close Panel</a>			
			</li>
	    	<li class="right">&nbsp;</li>
		</ul> 
	</div> <!-- / top -->
	
</div> <!--panel -->




	<div id="container">
    	<div id="title"><h1>Web & Script Programming Blog - Add Post</h1></div>
        <div id="subtitle"><h2>Michael Bourque - 100258740 - November 8th-12th, 2013<h2></div>
        <div id="navigation">
                 <a class="hmenu" href='addpost.php'>Add Post</a>
                 <a class="hmenu" href="admin.php">Cancel</a>
        </div>
        <div id="left">
        	<div id="blog">
				<form id="addPost" action="" method="POST">
                	<label for="blogtitle">Blog Title:</label>
    				<input type="text" name="blogtitle" value ="" class='blogtitleinput' />
                    <label for="blogcontents">Blog Content:</label>
                    <textarea name="blogcontents" id="bc_styled" class='blogcontentinput'></textarea>
		            <input type="submit" name="submit" value="Save" class="shoutbutton" />
                    <input type="button" name="cancel" value="Cancel" onclick="window.location='admin.php'" class="shoutbutton" />
                    
				</form>
            </div>
        </div>
        
        <div id="right">
        	<h2>Recent Blogs</h2>
       		<ul>
        	<?php
				$myQuery="SELECT id, blog_title FROM blogs ORDER BY date";
				$titleList=mysql_Query($myQuery, $con);
				
				while($row = mysql_fetch_array($titleList))
				{
					echo '<li><a class="bloglist" href="index.php?postid='.$row['id'].'">'.$row['blog_title'].'</a></li>';
				}
			?>
            </ul>
        </div>
        <div id="shoutbox">
        	<h2>Shout-outs:</h2>
            <br />
        	<?php
                $myQuery="SELECT * FROM shoutbox ORDER BY id";
				$shoutbox=mysql_Query($myQuery, $con);
				
				while($row = mysql_fetch_array($shoutbox))
				{
					echo "<div class='shoutout'><span class='shout_name'>".$row['username']."</span> said<br /><span class='shout'>". $row['shout'] . "</span></div>";
				}
        	?>
        </div>

        <br class="clear" />
	</div>
</body>
</html>