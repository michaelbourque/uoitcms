<?php

define('INCLUDE_CHECK',true);

require 'connect.php';
require 'functions.php';
// These two files can be included only if INCLUDE_CHECK is defined


session_name('ws_session');
// Starting the session

session_set_cookie_params(2*7*24*60*60);
// Making the cookie live for 2 weeks

session_start();

if($_SESSION['isAdmin'] != 1) 
{
	header("Location: index.php");
}

if($_SESSION['id'] && !isset($_COOKIE['wsRemember']) && !$_SESSION['rememberMe'])
{
	// If the user is logged in but does not have the 'Remember Me' selected, destory the session

	$_SESSION = array();
	session_destroy();
	
	// Destroy the session
}


if(isset($_GET['logoff']))
{
	$_SESSION = array();
	session_destroy();
	
	header("Location: index.php");
	exit;
}

$script = '';

if($_SESSION['msg'])
{
	// The script below shows the sliding panel on page load
	
	$script = '
	<script type="text/javascript">
	
		$(function(){
		
			$("div#panel").show();
			$("#toggle a").toggle();
		});
	
	</script>';
	
}
	

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Web&Script Programming Blog</title>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
<script src="http://code.jquery.com/jquery-latest.js" type="text/javascript"></script>
<script src="accordion.js" type="text/javascript"></script>
<script src="login_panel/js/slide.js" type="text/javascript"></script>
<link href="css/accordion.css" rel="stylesheet" type="text/css" media="screen">
<link href="css/styles.css" rel="stylesheet" type="text/css" />
<link href="css/panel.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="login_panel/css/slide.css" media="screen" />
<?php echo $script; ?>
  <script>
  $(function() {
    $( "#loglist" ).accordion();
  });
  </script>
</head>

<body>




<!-- Panel -->
<div id="toppanel">
	<div id="panel">
		<div class="content clearfix">
			<div class="left">
				<h1>Web&Script Blogging - Content Managment Portal</h1>
				<h2>Assignment #2</h2>		
				<p class="grey">Michael Bourque - 100258740</p>
				<h2>This system employs:</h2>
				<p class="grey">HTML, HTML5, CSS, CSS3, JavaScript, PHP, JQuery, AJAX, and more.</p>
			</div>
            
            
            <?php
			
			if(!$_SESSION['id']):
			
			?>
            
			<div class="left">
			</div>
			<div class="left right">			
			</div>
            
            <?php
			
			else:
			
			?>
            
            <div class="left">
            
            <h1>Members panel</h1>
            
            <p>Your are logged in as an authorized member of this blog.</p>
            <a href="?logoff">Log off</a>
            
            </div>
            
            <div class="left right">
            </div>
            
            <?php
			endif;
			?>
		</div>
	</div> <!-- /login -->	

    <!-- The tab on top -->	
	<div class="tab">
		<ul class="login">
	    	<li class="left">&nbsp;</li>
	        <li>Hello <?php echo $_SESSION['username'] ? $_SESSION['username'] : 'Guest';?>!</li>
			<li class="sep">|</li>
			<li id="toggle">
				<a id="open" class="open" href="#"><?php echo $_SESSION['id']?'Open Panel':'Log In | Register';?></a>
				<a id="close" style="display: none;" class="close" href="#">Close Panel</a>			
			</li>
	    	<li class="right">&nbsp;</li>
		</ul> 
	</div> <!-- / top -->
	
</div> <!--panel -->




	<div id="container">
    	<div id="title"><h1>Web & Script Programming Blog - Content Management Portal</h1></div>
        <div id="subtitle"><h2>Michael Bourque - 100258740 - November 8th-12th, 2013<h2></div>
        <div id="navigation">
                 <a class="hmenu" href='admin.php'>Admin</a>
                 <a class="hmenu" href="index.php">Home</a>
        </div>
        <div id="logleft">
        	<div id="log">
            	<div id="accordion-container">
				<?php
					$myQuery="SELECT * FROM logs";
					$logdata=mysql_query($myQuery, $con);
					
					while($row = mysql_fetch_array($logdata)) {
						?>
						<h2 class="accordion-header"><?php echo $row['date'] . ' ' . $row['time'] . ' ' . $row['ip'];?></h2>
                        <div class="accordion-content">
                        	<?php if($row['referrer']){ ?><p><strong>Referrer:</strong> <?php echo $row['referrer']; ?></p><?php  } ?>
                            <?php if($row['agent']){ ?><p><strong>Agent:</strong> <?php echo $row['agent']; ?></p><?php  } ?>
                            <?php if($row['host']){ ?><p><strong>Host:</strong> <?php echo $row['host']; ?></p><?php  } ?>
                            
                            <?php $myQuery="SELECT blog_title FROM blogs WHERE id='".$row['postid']."'";
								  $posttitle=mysql_query($myQuery, $con);
								  $post = mysql_fetch_array($posttitle) ?>
                            
                            <?php if($row['postid']){ ?><p><strong>Post:</strong> <?php echo $row['postid']; ?> ( <a href='index.php?postid=<?php echo $row['postid'];?>'><?php echo $post['blog_title']; ?></a> )</p><?php  } ?>
                        </div>
					<?php	
					}
				?>
                </div>
            </div>
        </div>
        <br class="clear" />
	</div>
</body>
</html>



